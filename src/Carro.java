public class Carro {

    private Integer quantidadePneus;
    private Integer quantidadeCalotas;
    private Integer quantidadeParafusosPneu;
    private Integer quantidadePortas;
    private String cor;
    private String tecidoBanco;
    private String tipoCambio;
    private String tipoDirecao;
    private String arCondicionado;
    private String tipoCombustivel;

    public Carro(Integer quantidadePneus,
                 String cor,
                 String tipoCambio,
                 Integer quantidadePortas,
                 String tipoCombustivel) {
        setQuantidadePneus(quantidadePneus);
        setTipoCambio(tipoCambio);
        setCor(cor);
        setQuantidadePortas(quantidadePortas);
        setTipoCombustivel(tipoCombustivel);
    }

    public Integer getQuantidadePneus() {
        return quantidadePneus + 1;
    }

    public void setQuantidadePneus(Integer quantidadePneus) {
        this.quantidadePneus = quantidadePneus;
        setQuantidadeCalotas(quantidadePneus);
        setQuantidadeParafusosPneu(quantidadePneus * 4);
    }

    public Integer getQuantidadeCalotas() {
        return quantidadeCalotas;
    }

    public void setQuantidadeCalotas(Integer quantidadeCalotas) {
        this.quantidadeCalotas = quantidadeCalotas;
    }

    public Integer getQuantidadeParafusosPneu() {
        return quantidadeParafusosPneu;
    }

    public void setQuantidadeParafusosPneu(Integer quantidadeParafusosPneu) {
        this.quantidadeParafusosPneu = quantidadeParafusosPneu;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getTecidoBanco() {
        return tecidoBanco;
    }

    public void setTecidoBanco(String tecidoBanco) {
        this.tecidoBanco = tecidoBanco;
    }

    public String getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(String tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public String getTipoDirecao() {
        return tipoDirecao;
    }

    public void setTipoDirecao(String tipoDirecao) {
        this.tipoDirecao = tipoDirecao;
    }

    public String getArCondicionado() {
        return arCondicionado;
    }

    public void setArCondicionado(String arCondicionado) {
        this.arCondicionado = arCondicionado;
    }

    public Integer getQuantidadePortas() {
        return quantidadePortas;
    }

    public void setQuantidadePortas(Integer quantidadePortas) {
        this.quantidadePortas = quantidadePortas;
    }

    public String getTipoCombustivel() {
        return tipoCombustivel;
    }

    public void setTipoCombustivel(String tipoCombustivel) {
        this.tipoCombustivel = tipoCombustivel;
    }

    public void imprimeValores(){
        System.out.println("Quantidade de pneus " + getQuantidadePneus());
        System.out.println("Quantidade de calotas " + getQuantidadeCalotas());
        System.out.println("Quantidade de parafusos " + getQuantidadeParafusosPneu());
        System.out.println("A cor desejada para o carro é " + getCor());
        System.out.println("o tipo de cambio do carro é " + getTipoCambio());
        System.out.println("Este carro possui ar-condicionado? " + getArCondicionado());
    }
}
